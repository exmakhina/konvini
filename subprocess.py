#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2016,2022 Jérôme Carretero <cJ@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT

import os
import subprocess
import logging
import fcntl
import signal

class TerminatingPopen(subprocess.Popen):
	"""
	Like subprocess.Popen but ensures the subprocess is terminated
	when __exit__ is called.
	"""
	def __init__(self, cmd, **kw):
		if os.name == "nt":
			kw.setdefault("creationflags", subprocess.CREATE_NEW_PROCESS_GROUP)
		super().__init__(cmd, **kw)

	def __exit__(self, *args):
		if os.name == "nt":
			self.terminate()
			os.kill(self.pid, signal.CTRL_BREAK_EVENT)
		else:
			super().__exit__(*args)


def fcntl_nonblocking(fd):
	flag_ = fcntl.fcntl(fd, fcntl.F_GETFL)
	fcntl.fcntl(fd, fcntl.F_SETFL, flag_ | os.O_NONBLOCK)
	flag = fcntl.fcntl(fd, fcntl.F_GETFL)
	if flag & os.O_NONBLOCK == 0:
		raise RuntimeError(f"Couldn't put {fd} as non-blocking")
	return flag_

